# tacon-data-pouring

Project to simplify the transfer of data from one source to another

## Getting started

The main class is `Copy`, and in particular the method `copy(Input<TI> input, Output<TO> output, InputConverter<TI, TO> converter)` that reads data from `input`, transforms it by `converter`, and writes it to `output`

### Input
Input is an interface that has two methods: 
* `boolean hasNext()` which indicates if there are other values to read
* `T read()` which reads a single value

The `DatabaseInput` implementation facilitates reading from relational databases.

### Output
Output is an interface with a `write(T data, int readCount, int writeCount)` method that writes a single value.

`DatabaseOutput` is an implementation that simplifies writing to relational databases.

### InputConverter
`InputConverter` is an interface to transform input value into output value.