package dev.tacon.datapouring;

import java.util.NoSuchElementException;

public abstract class AbstractInput<T> implements Input<T> {

	private int readCount = 0;

	private Boolean next = null;

	@Override
	public boolean hasNext() throws InputReadException {
		if (this.next != null) {
			return this.next.booleanValue();
		}
		final boolean _next = this.checkNext(this.readCount);
		this.next = Boolean.valueOf(_next);
		return _next;
	}

	@Override
	public T read() throws InputReadException {
		if (this.next == null) {
			if (!this.hasNext()) {
				throw new NoSuchElementException();
			}
		}
		this.next = null;
		this.readCount++;
		return this.doRead(this.readCount);
	}

	/**
	 *
	 * @param _readCount
	 * @return
	 * @throws InputReadException
	 */
	protected abstract boolean checkNext(int _readCount) throws InputReadException;

	/**
	 *
	 * @param _readCount
	 * @return
	 * @throws InputReadException
	 */
	protected abstract T doRead(int _readCount) throws InputReadException;

}
