package dev.tacon.datapouring;

@FunctionalInterface
public interface InputConverter<T, R> {

	R apply(T t) throws Exception;
}