package dev.tacon.datapouring;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.function.BiFunction;

import dev.tacon.datapouring.impl.DatabaseInput;
import dev.tacon.datapouring.impl.StringSeparatedInput;

public interface Input<T> extends AutoCloseable {

	boolean hasNext() throws Exception;

	T read() throws Exception;

	static Input<Object[]> fromDatabase(final Connection connection, final String query, final Object... params) throws SQLException {
		return new DatabaseInput<>(connection, query, (i, o) -> o, params);
	}

	static <T> Input<T> fromDatabase(final Connection connection, final String query, final BiFunction<DatabaseInput<T>, Object[], T> converter, final Object... params) throws SQLException {
		return new DatabaseInput<>(connection, query, converter, params);
	}

	static Input<String[]> fromTabSeparatedInputStream(final InputStream is) {
		return new StringSeparatedInput("\t", is);
	}

}
