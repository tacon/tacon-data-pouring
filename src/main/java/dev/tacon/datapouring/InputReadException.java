package dev.tacon.datapouring;

public class InputReadException extends Exception {

	private static final long serialVersionUID = -113254040489394556L;

	private final int readCount;

	public InputReadException(final Throwable cause, final int readCount) {
		super(cause);
		this.readCount = readCount;
	}

	public InputReadException(final String message, final int readCount) {
		super(message);
		this.readCount = readCount;
	}

	public InputReadException(final String message, final Throwable cause, final int readCount) {
		super(message, cause);
		this.readCount = readCount;
	}

	public int getReadCount() {
		return this.readCount;
	}
}
