package dev.tacon.datapouring;

public class OutputWriteException extends Exception {

	private static final long serialVersionUID = 2691795679650556273L;

	private final int readCount;
	private final int writeCount;

	public OutputWriteException(final Throwable cause, final int readCount, final int writeCount) {
		super(cause);
		this.readCount = readCount;
		this.writeCount = writeCount;
	}

	public OutputWriteException(final String message, final int readCount, final int writeCount) {
		super(message);
		this.readCount = readCount;
		this.writeCount = writeCount;
	}

	public OutputWriteException(final String message, final Throwable cause, final int readCount, final int writeCount) {
		super(message, cause);
		this.readCount = readCount;
		this.writeCount = writeCount;
	}

	public int getReadCount() {
		return this.readCount;
	}

	public int getWriteCount() {
		return this.writeCount;
	}
}