package dev.tacon.datapouring.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import dev.tacon.datapouring.Output;

public class MapDatabaseOutput implements Output<Map<String, Object>> {

	protected final List<String> fields;
	protected final int executeBatchThreshold;
	protected final String tableName;

	private final PreparedStatement ps;

	public MapDatabaseOutput(final Connection connection, final String tableName, final String... fields) throws SQLException {
		this(connection, tableName, Arrays.asList(fields));
	}

	public MapDatabaseOutput(final Connection connection, final int executeBatchThreshold, final String tableName, final String... fields) throws SQLException {
		this(connection, executeBatchThreshold, tableName, Arrays.asList(fields));
	}

	public MapDatabaseOutput(final Connection connection, final String tableName, final List<String> fields) throws SQLException {
		this(connection, -1, tableName, fields);
	}

	public MapDatabaseOutput(final Connection connection, final int executeBatchThreshold, final String tableName, final List<String> fields) throws SQLException {
		this.tableName = tableName;
		this.ps = connection.prepareStatement("insert into " + tableName + " (" + fields.stream().collect(Collectors.joining(",")) + ") values (" + fields.stream().map(x -> "?").collect(Collectors.joining(",")) + ")");
		this.fields = Collections.unmodifiableList(fields);
		this.executeBatchThreshold = executeBatchThreshold;
	}

	@Override
	public void write(final Map<String, Object> data, final int readCount, final int writeCount) throws Exception {
		for (final String field : this.fields) {
			final Object value = this.getValue(data, field);
			this.setObject(this.ps, field, value);
		}
		if (this.executeBatchThreshold < 0) {
			this.ps.executeUpdate();
		} else {
			this.ps.addBatch();
			if (this.executeBatchThreshold > 0) {
				if (writeCount > 0 && writeCount % this.executeBatchThreshold == 0) {
					this.ps.executeBatch();
				}
			}
		}
		this.postWrite(data, readCount, writeCount);
	}

	protected Object getValue(final Map<String, Object> data, final String field) {
		return data.get(field);
	}

	protected void setObject(final PreparedStatement ps, final String field, final Object value) throws SQLException {
		ps.setObject(this.getFieldPos(field), value);
	}

	protected int getFieldPos(final String field) {
		int i = 1;
		for (final String f : this.fields) {
			if (f.equalsIgnoreCase(field)) {
				return i;
			}
			i++;
		}
		return -1;
	}

	/**
	 *
	 * @param data
	 * @param readCount
	 * @param writeCount
	 */
	protected void postWrite(final Map<String, Object> data, final int readCount, final int writeCount) {}

	@Override
	public void close() {
		try {
			if (this.executeBatchThreshold >= 0) {
				this.ps.executeBatch();
			}
			this.ps.close();
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		}
	}

}