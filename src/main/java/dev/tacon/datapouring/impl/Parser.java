package dev.tacon.datapouring.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class Parser {

	static String[] split(final String line, final String separator, final Character quoteCharacter, final Character escapeCharacter, final String escapeSequence) {
		Objects.requireNonNull(line, "line is null");
		Objects.requireNonNull(separator, "separator is null");
		if (quoteCharacter == null && escapeCharacter == null) {
			return parse(line, separator);
		}
		if (quoteCharacter != null && escapeCharacter == null) {
			return parse(line, separator, quoteCharacter);
		}
		if (quoteCharacter == null && escapeCharacter != null) {
			Objects.requireNonNull(escapeSequence, "escapeSequence is null");
			return parse(line, separator, escapeCharacter, escapeSequence);
		}
		if (quoteCharacter != null && escapeCharacter != null) {
			Objects.requireNonNull(escapeSequence, "escapeSequence is null");
			return parse(line, separator, quoteCharacter, escapeCharacter, escapeSequence);
		}
		throw new IllegalStateException();
	}

	private static String[] parse(final String line, final String separator, final Character quoteCharacter, final Character escapeCharacter, final String escapeSequence) {
		final int length = line.length();
		if (length == 0) {
			return new String[] { "" };
		}
		// TODO
		final List<String> list = new ArrayList<>();
		final char escapeChar = escapeCharacter.charValue();
		final char quoteChar = quoteCharacter.charValue();
		final StringBuilder sb = new StringBuilder();
		boolean isEscape = false;
		boolean isQuote = false;
		for (int i = 0; i < length; i++) {
			final char c = line.charAt(i);
			if (isEscape) {
				isEscape = false;
				sb.append(c);
			} else if (isQuote) {
				if (line.indexOf(escapeSequence, i - 1) == i/* migliorare */) {
					sb.append(escapeChar);
					i = i + escapeSequence.length() - 1;
				} else if (c == escapeChar) {
					isEscape = true;
				} else if (c == quoteChar && (line.indexOf(separator, i)/* migliorare */ == i + 1 || i == length - 1)) {
					isQuote = false;
					list.add(sb.toString());
					sb.delete(0, sb.length());
				} else {
					sb.append(c);
				}
			} else {
				if (line.indexOf(escapeSequence, i - 1) == i/* migliorare */) {
					sb.append(escapeChar);
					i = i + escapeSequence.length() - 1;
				} else if (c == escapeChar) {
					isEscape = true;
				} else if (c == quoteChar) {
					isQuote = true;
				} else {
					if (line.indexOf(separator, i - 1) == i) {
						list.add(sb.toString());
						sb.delete(0, sb.length());
						i = i + separator.length() - 1;
					} else {
						sb.append(c);
						if (i == length - 1) {
							list.add(sb.toString());
						}
					}
				}
			}
		}
		return list.toArray(new String[list.size()]);
	}

	private static String[] parse(final String line, final String separator, final Character escapeCharacter, final String escapeSequence) {
		final int length = line.length();
		final List<String> list = new ArrayList<>();
		final char escapeChar = escapeCharacter.charValue();
		boolean isEscape = false;
		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			final char c = line.charAt(i);
			if (isEscape) {
				isEscape = false;
				sb.append(c);
			} else {
				if (line.indexOf(escapeSequence, i - 1) == i) {
					sb.append(escapeChar);
					i = i + escapeSequence.length() - 1;
				} else if (c == escapeChar) {
					isEscape = true;
				} else {
					if (line.indexOf(separator, i - 1) == i) {
						list.add(sb.toString());
						sb.delete(0, sb.length());
						i = i + separator.length() - 1;
					} else {
						sb.append(c);
					}
				}
			}
		}
		list.add(sb.toString());
		return list.toArray(new String[list.size()]);
	}

	private static String[] parse(final String line, final String separator, final Character quoteCharacter) {
		final int length = line.length();
		final List<String> list = new ArrayList<>();
		final char quoteChar = quoteCharacter.charValue();
		boolean isQuote = false;
		boolean lastQuote = false;
		int separatorIndex = 0;
		int begin = 0;
		for (int i = 0; i < length; i++) {
			final char c = line.charAt(i);
			if (isQuote) {
				if (c == quoteChar && (line.indexOf(separator, i) == i + 1 || i == length - 1)) {
					isQuote = false;
					lastQuote = true;
				}
			} else {
				if (c == separator.charAt(separatorIndex)) {
					separatorIndex++;
					if (separatorIndex == separator.length()) {
						list.add(line.substring(begin, i - separatorIndex + (lastQuote ? 0 : 1)));
						begin = i + 1;
						separatorIndex = 0;
						isQuote = false;
					}
				} else {
					lastQuote = false;
					separatorIndex = 0;
					if (c == quoteChar && begin == i) {
						isQuote = true;
						begin++;
					}
				}
			}
		}
		list.add(line.substring(begin, length + (lastQuote ? -1 : 0)));
		return list.toArray(new String[list.size()]);
	}

	private static String[] parse(final String line, final String separator) {
		final List<String> list = new ArrayList<>();
		int last = 0;
		int index = 0;
		while ((index = line.indexOf(separator, last)) >= 0) {
			list.add(line.substring(last, index));
			last = index + separator.length();
		}
		list.add(line.substring(last));
		return list.toArray(new String[list.size()]);
	}

}
