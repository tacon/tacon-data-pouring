package dev.tacon.datapouring.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

import dev.tacon.datapouring.AbstractInput;
import dev.tacon.datapouring.InputReadException;

public class StringSeparatedInput extends AbstractInput<String[]> {

	private final InputStreamReader inputStreamReader;
	private final BufferedReader bufferedReader;
	private final String separator;

	private Character quoteCharacter;
	private Character escapeCharacter;
	private String escapeSequence;

	private String line = null;

	public StringSeparatedInput(final String separator, final InputStream is) {
		Objects.requireNonNull(separator, "separator is null");
		if (separator.isEmpty()) {
			throw new IllegalArgumentException("separator is empty");
		}
		this.inputStreamReader = new InputStreamReader(is);
		this.separator = separator;
		this.bufferedReader = new BufferedReader(this.inputStreamReader);
	}

	@Override
	protected boolean checkNext(final int readCount) throws InputReadException {
		try {
			this.line = this.bufferedReader.readLine();
			return this.line != null;
		} catch (final IOException e) {
			throw new InputReadException(e, readCount);
		}
	}

	@Override
	public String[] doRead(final int readCount) {
		if (this.line == null) {
			return null;
		}
		if (this.line.isEmpty()) {
			return new String[0];
		}

		return split(this.line, this.separator, this.quoteCharacter, this.escapeCharacter, this.escapeSequence);

	}

	private static String[] split(final String line, final String separator, final Character quoteCharacter, final Character escapeCharacter, final String escapeSequence) {
		return Parser.split(line, separator, quoteCharacter, escapeCharacter, escapeSequence);
	}

	@Override
	public void close() throws IOException {
		try {
			this.bufferedReader.close();
		} catch (final Exception e) {
			try {
				this.inputStreamReader.close();
			} catch (final Exception e1) {
				e1.addSuppressed(e);
				throw e1;
			}
			throw e;
		}
		this.inputStreamReader.close();
	}

}
