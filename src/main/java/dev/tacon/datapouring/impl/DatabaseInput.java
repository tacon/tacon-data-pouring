package dev.tacon.datapouring.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

import dev.tacon.datapouring.AbstractInput;
import dev.tacon.datapouring.InputReadException;

public class DatabaseInput<T> extends AbstractInput<T> {

	private final PreparedStatement prepareStatement;
	private final ResultSet rs;
	private final int columnCount;
	private final Map<String, Integer> columnMap = new HashMap<>();
	private final List<String> fields;
	private final BiFunction<DatabaseInput<T>, Object[], T> converter;

	public DatabaseInput(final Connection connection, final String query, final BiFunction<DatabaseInput<T>, Object[], T> converter, final Object... params) throws SQLException {
		this.prepareStatement = connection.prepareStatement(query);
		for (int i = 0; i < params.length; i++) {
			this.prepareStatement.setObject(1 + i, params[i]);
		}
		this.rs = this.prepareStatement.executeQuery();
		this.columnCount = this.rs.getMetaData().getColumnCount();
		final List<String> f = new ArrayList<>(this.columnCount);
		for (int i = 1; i <= this.columnCount; i++) {
			final String columnLabel = this.rs.getMetaData().getColumnLabel(i);
			if (columnLabel != null && !columnLabel.trim().isEmpty()) {
				this.columnMap.put(columnLabel.toUpperCase(), Integer.valueOf(i));
			}
			f.add(columnLabel);
		}
		this.fields = Collections.unmodifiableList(f);
		this.converter = converter;
	}

	public List<String> getFields() {
		return this.fields;
	}

	public Object getValue(final String columnName, final Object[] values) {
		final Integer pos = this.columnMap.get(columnName.toUpperCase());
		if (pos == null) {
			throw new RuntimeException("Column not found");
		}
		return values[pos.intValue() - 1];
	}

	@Override
	public boolean checkNext(final int readCount) throws InputReadException {
		try {
			return this.rs.next();
		} catch (final SQLException e) {
			throw new InputReadException(e, readCount);
		}
	}

	@Override
	public T doRead(final int readCount) throws InputReadException {
		final Object[] result = new Object[this.columnCount];
		for (int i = 0; i < this.columnCount; i++) {
			try {
				result[i] = this.getObject(this.rs, i);
			} catch (final SQLException e) {
				throw new InputReadException(e, readCount);
			}
		}
		return this.convert(result);
	}

	protected Object getObject(final ResultSet resultSet, final int i) throws SQLException {
		return resultSet.getObject(i + 1);
	}

	private T convert(final Object[] result) {
		return this.converter.apply(this, result);
	}

	@Override
	public void close() throws Exception {
		try {
			this.rs.close();
		} catch (final Exception e) {
			try {
				this.prepareStatement.close();
			} catch (final Exception e1) {
				e1.addSuppressed(e);
				throw e1;
			}
			throw e;
		}
		this.prepareStatement.close();
	}

}
