package dev.tacon.datapouring.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import dev.tacon.datapouring.Output;

public class ResultSetDatabaseOutput implements Output<Object[]> {

	private final ResultSet resultSet;
	private final int fieldsCount;

	public ResultSetDatabaseOutput(final ResultSet resultSet) throws SQLException {
		this.resultSet = resultSet;
		this.fieldsCount = resultSet.getMetaData().getColumnCount();
	}

	@Override
	public void write(final Object[] data, final int readCount, final int writeCount) throws Exception {
		if (data.length != this.fieldsCount) {
			throw new Exception("data.length (" + data.length + ") != fieldsCount (" + this.fieldsCount + ")");
		}
		this.resultSet.moveToInsertRow();
		for (int i = 0; i < data.length; i++) {
			this.resultSet.updateObject(i + 1, data[i]);
		}
		this.resultSet.insertRow();
	}

	@Override
	public void close() {
		// resultSet will close by caller
	}

}
