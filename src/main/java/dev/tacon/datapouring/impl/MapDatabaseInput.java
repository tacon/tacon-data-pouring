package dev.tacon.datapouring.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

public class MapDatabaseInput extends DatabaseInput<Map<String, Object>> {

	public MapDatabaseInput(final Connection connection, final String query, final Object... params) throws SQLException {
		super(connection, query, copyFunction(), params);
	}

	private static BiFunction<DatabaseInput<Map<String, Object>>, Object[], Map<String, Object>> copyFunction() {
		return (i, o) -> {
			final Map<String, Object> result = new HashMap<>();
			for (final String field : i.getFields()) {
				result.put(field, i.getValue(field, o));
			}
			return result;
		};
	}
}
