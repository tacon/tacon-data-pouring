package dev.tacon.datapouring.impl;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.stream.Collectors;

import dev.tacon.datapouring.Output;

public class StringSeparatedOutput implements Output<String[]> {

	private final String separator;
	private final OutputStreamWriter outputStreamWriter;
	private final BufferedWriter bufferedWriter;

	private String newLineSeparator;
	private Character quoteCharacter;
	private Character escapeCharacter;
	private String escapeSequence;

	public StringSeparatedOutput(final OutputStream os, final String separator) {
		this.separator = separator;
		this.outputStreamWriter = new OutputStreamWriter(os);
		this.bufferedWriter = new BufferedWriter(this.outputStreamWriter);
	}

	public StringSeparatedOutput setNewLineSeparator(final String newLineSeparator) {
		this.newLineSeparator = newLineSeparator;
		return this;
	}

	public StringSeparatedOutput setEscape(final Character escapeCharacter, final String escapeSequence) {
		this.escapeCharacter = escapeCharacter;
		this.escapeSequence = escapeSequence;
		if (escapeCharacter == null != (escapeSequence == null)) {
			throw new IllegalArgumentException();
		}
		return this;
	}

	public StringSeparatedOutput setQuoteCharacter(final Character quoteCharacter) {
		this.quoteCharacter = quoteCharacter;
		return this;
	}

	@Override
	public void write(final String[] data, final int readCount, final int writeCount) throws Exception {
		final String s = Arrays.stream(data).map(this::escape).collect(Collectors.joining(this.separator));
		this.bufferedWriter.append(s);
		if (this.newLineSeparator == null) {
			this.bufferedWriter.newLine();
		} else {
			this.bufferedWriter.append(this.newLineSeparator);
		}

	}

	protected String escape(final String s) {
		if (this.quoteCharacter == null && this.escapeCharacter == null) {
			return s;
		}
		if (this.quoteCharacter != null && this.escapeCharacter != null) {
			return this.quoteCharacter +
					s.replace(this.escapeCharacter.toString(), this.escapeSequence)
							.replace(this.quoteCharacter.toString(), this.escapeCharacter.toString() + this.quoteCharacter) +
					this.quoteCharacter;
		}
		if (this.quoteCharacter != null && this.escapeCharacter == null) {
			return this.quoteCharacter + s + this.quoteCharacter;
		}
		// (this.quoteCharacter == null && this.escapeCharacter != null)
		return s.replace(this.escapeCharacter.toString(), this.escapeSequence)
				.replace(this.separator.toString(), this.escapeCharacter.toString() + this.separator);

	}

	@Override
	public void close() throws Exception {
		try {
			this.bufferedWriter.close();
		} catch (final Exception e) {
			try {
				this.outputStreamWriter.close();
			} catch (final Exception e1) {
				e1.addSuppressed(e);
				throw e1;
			}
			throw e;
		}
		this.outputStreamWriter.close();
	}

}
