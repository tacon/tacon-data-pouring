package dev.tacon.datapouring.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.stream.Collectors;

import dev.tacon.datapouring.Output;

public class DatabaseOutput implements Output<Object[]> {

	protected final int fieldsCount;
	protected final int executeBatchThreshold;
	protected final String tableName;

	private final PreparedStatement ps;

	public DatabaseOutput(final Connection connection, final String tableName, final String... fields) throws SQLException {
		this(connection, -1, tableName, fields);
	}

	public DatabaseOutput(final Connection connection, final int executeBatchThreshold, final String tableName, final String... fields) throws SQLException {
		this.tableName = tableName;
		this.ps = connection.prepareStatement("insert into " + tableName + " (" + Arrays.stream(fields).collect(Collectors.joining(",")) + ") values (" + Arrays.stream(fields).map(x -> "?").collect(Collectors.joining(",")) + ")");
		this.fieldsCount = fields.length;
		this.executeBatchThreshold = executeBatchThreshold;
	}

	@Override
	public void write(final Object[] data, final int readCount, final int writeCount) throws Exception {
		this.validateData(data);
		for (int i = 0; i < data.length; i++) {
			this.setObject(this.ps, data, i);
		}
		if (this.executeBatchThreshold < 0) {
			this.ps.executeUpdate();
		} else {
			this.ps.addBatch();
			if (this.executeBatchThreshold > 0) {
				if (writeCount > 0 && writeCount % this.executeBatchThreshold == 0) {
					this.ps.executeBatch();
				}
			}
		}
		this.postWrite(data, readCount, writeCount);
	}

	protected void validateData(final Object[] data) throws Exception {
		if (data.length != this.fieldsCount) {
			throw new Exception("data.length (" + data.length + ") != fieldsCount (" + this.fieldsCount + ")");
		}
	}

	protected void setObject(final PreparedStatement ps, final Object[] data, final int i) throws SQLException {
		ps.setObject(i + 1, data[i]);
	}

	/**
	 * @param data
	 * @param readCount
	 * @param writeCount
	 */
	protected void postWrite(final Object[] data, final int readCount, final int writeCount) {}

	@Override
	public void close() {
		try {
			if (this.executeBatchThreshold >= 0) {
				this.ps.executeBatch();
			}
			this.ps.close();
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		}
	}

}
