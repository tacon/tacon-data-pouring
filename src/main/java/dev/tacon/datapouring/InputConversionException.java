package dev.tacon.datapouring;

public class InputConversionException extends Exception {

	private static final long serialVersionUID = 7068338748953443132L;

	private final int readCount;

	public InputConversionException(final Throwable cause, final int readCount) {
		super(cause);
		this.readCount = readCount;
	}

	public InputConversionException(final String message, final int readCount) {
		super(message);
		this.readCount = readCount;
	}

	public InputConversionException(final String message, final Throwable cause, final int readCount) {
		super(message, cause);
		this.readCount = readCount;
	}

	public int getReadCount() {
		return this.readCount;
	}
}
