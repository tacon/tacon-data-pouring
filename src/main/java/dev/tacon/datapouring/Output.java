package dev.tacon.datapouring;

import java.sql.Connection;
import java.sql.SQLException;

import dev.tacon.datapouring.impl.DatabaseOutput;

public interface Output<T> extends AutoCloseable {

	void write(T data, final int readCount, final int writeCount) throws Exception;

	static Output<Object[]> toDatabase(final Connection connection, final String tableName, final String... fields) throws SQLException {
		return new DatabaseOutput(connection, tableName, fields);
	}
}
