package dev.tacon.datapouring;

import java.util.function.Predicate;

public class Copy<TI, TO> {

	public static <TI, TO> CopyResult copy(final Input<TI> input, final Output<TO> output, final InputConverter<TI, TO> converter) throws InputReadException, InputConversionException, OutputWriteException {
		return new Copy<>(converter).copy(input, output);
	}

	public static class CopyResult {

		private final int read;
		private final int converted;
		private final int written;

		protected CopyResult(final int read, final int converted, final int written) {
			this.read = read;
			this.converted = converted;
			this.written = written;
		}

		public int getRead() {
			return this.read;
		}

		public int getConverted() {
			return this.converted;
		}

		public int getWritten() {
			return this.written;
		}

	}

	private final InputConverter<TI, TO> converter;
	private final Predicate<TI> convertible;
	private final Predicate<TO> writable;

	public Copy(final InputConverter<TI, TO> converter) {
		this(converter, i -> true, o -> true);
	}

	public Copy(final InputConverter<TI, TO> converter, final Predicate<TI> convertible, final Predicate<TO> writable) {
		this.converter = converter;
		this.convertible = convertible;
		this.writable = writable;
	}

	public CopyResult copy(final Input<TI> input, final Output<TO> output) throws InputReadException, InputConversionException, OutputWriteException {
		int readCount = 0;
		int convertCount = 0;
		int writeCount = 0;
		while (this.safeNext(input, readCount)) {
			final TI inputData = this.read(input, readCount);
			if (this.convertible.test(inputData)) {
				final TO convertedData = this.convert(this.converter, inputData, readCount);
				if (this.writable.test(convertedData)) {
					this.write(output, convertedData, readCount, writeCount);
					writeCount++;
				}
				convertCount++;
			}
			readCount++;
		}
		return new CopyResult(readCount, convertCount, writeCount);
	}

	protected InputConverter<TI, TO> getConverter() {
		return this.converter;
	}

	protected void write(final Output<TO> output, final TO convertedData, final int readCount, final int writeCount) throws OutputWriteException {
		try {
			output.write(convertedData, readCount, writeCount);
		} catch (final Exception e) {
			throw new OutputWriteException(e, readCount, writeCount);
		}
	}

	protected TO convert(final InputConverter<TI, TO> ioConverter, final TI inputData, final int readCount) throws InputConversionException {
		try {
			return ioConverter.apply(inputData);
		} catch (final Exception e) {
			throw new InputConversionException(e, readCount);
		}
	}

	protected TI read(final Input<TI> input, final int readCount) throws InputReadException {
		try {
			return input.read();
		} catch (final Exception e1) {
			throw new InputReadException(e1, readCount);
		}
	}

	protected boolean safeNext(final Input<TI> input, final int readCount) throws InputReadException {
		try {
			return input.hasNext();
		} catch (final Exception e) {
			throw new InputReadException(e, readCount);
		}
	}
}
