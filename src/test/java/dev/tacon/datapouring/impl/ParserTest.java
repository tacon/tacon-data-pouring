package dev.tacon.datapouring.impl;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class ParserTest {

	@Test
	public void testSeparator() {
		final String separator = "\t";
		test("", separator, null, null, null, "");
		test("ciao" + separator + "ciao", separator, null, null, null, "ciao", "ciao");
		test("ciao" + separator + "\"ciao", separator, null, null, null, "ciao", "\"ciao");
	}

	@Test
	public void testSeparator1Quoting() {
		testSeparatorQuoting("\t", Character.valueOf('"'));
	}

	@Test
	public void testSeparator3Quoting() {
		testSeparatorQuoting("$-$", Character.valueOf('|'));
	}

	private static void testSeparatorQuoting(final String sep1, final Character quoteChar) {
		test("", sep1, quoteChar, null, null, "");
		test("ciao" + sep1 + "ciao", sep1, quoteChar, null, null, "ciao", "ciao");
		test("ciao" + sep1 + quoteChar + "ciao" + quoteChar, sep1, quoteChar, null, null, "ciao", "ciao");
		test("ciao" + sep1 + quoteChar + "ciao" + quoteChar + sep1 + quoteChar + "ciao", sep1, quoteChar, null, null, "ciao", "ciao", "ciao");
		test("ciao" + sep1 + quoteChar + "ciao" + quoteChar + sep1 + quoteChar + "ci\"ao", sep1, quoteChar, null, null, "ciao", "ciao", "ci\"ao");
		test("ciao" + sep1 + quoteChar + "ci\"ao" + quoteChar + sep1 + quoteChar + "ci\"ao" + quoteChar, sep1, quoteChar, null, null, "ciao", "ci\"ao", "ci\"ao");
	}

	@Test
	public void testSeparatorEscape() {
		final String sep1 = "\t";
		final Character escC = Character.valueOf('\\');
		final String escS = "\\\\";
		test("", sep1, null, escC, escS, "");
		test("ciao" + sep1 + "come", sep1, null, escC, escS, "ciao", "come");
		test("ciao" + sep1 + "ciao", sep1, null, escC, escS, "ciao", "ciao");
		test("ciao" + sep1 + "ciao", sep1, null, escC, escS, "ciao", "ciao");
		test("ciao" + sep1 + "ciao" + sep1 + "ciao", sep1, null, escC, escS, "ciao", "ciao", "ciao");
		test("ciao" + sep1 + "ciao" + sep1 + "ci\"ao", sep1, null, escC, escS, "ciao", "ciao", "ci\"ao");
		test("ciao" + sep1 + "ci\"ao" + sep1 + "ci\"ao", sep1, null, escC, escS, "ciao", "ci\"ao", "ci\"ao");
		test("ciao" + sep1 + "ci" + escC + sep1 + "ao", sep1, null, escC, escS, "ciao", "ci" + sep1 + "ao");
		test("ciao" + sep1 + "ci" + escC + "ao", sep1, null, escC, escS, "ciao", "ci" + "ao");
		test("ciao" + sep1 + "ci" + escC + sep1 + "ao" + sep1 + "c" + escS + escC + sep1 + "iao", sep1, null, escC, escS, "ciao", "ci" + sep1 + "ao", "c" + escC + sep1 + "iao");
		test("ciao" + sep1 + "ci" + escC + sep1 + "ao" + sep1 + "c" + escS + sep1 + "iao", sep1, null, escC, escS, "ciao", "ci" + sep1 + "ao", "c" + escC, "iao");

	}

	@Test
	public void testSeparatorQuotingEscape() {
		// final String sep1 = "\t";
		// final Character quoteC = Character.valueOf('"');
		// final Character escC = Character.valueOf('\\');
		// final String escS = "\\\\";
		// test("", sep1, quoteC, escC, escS, "");
		// test("ciao", sep1, quoteC, escC, escS, "ciao");
		// test("ciao" + sep1 + "come", sep1, quoteC, escC, escS, "ciao", "come");
		// test(quoteC + "ciao" + quoteC + sep1 + quoteC + "come" + quoteC, sep1, quoteC, escC, escS, "ciao", "come");

	}

	private static void test(final String line, final String separator, final Character quoteCharacter, final Character escapeCharacter, final String escapeSequence, final String... result) {
		final String[] r = Parser.split(line, separator, quoteCharacter, escapeCharacter, escapeSequence);
		assertArrayEquals(r, result);
	}
}